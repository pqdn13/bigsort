Библеотечные функции

Arrays.stream(ints).parallel().sorted().toArray()
12.661

Arrays.stream(ints).sorted().toArray()
3,404 sec

-----

Чистая быстрая сортировка
AbstractQuckSort quckSort = new QuickSort();
15,185 sec

Распаралелиная быстрая сортировка + сортировка пузырьком (size <= 16)
AbstractQuckSort quickSort = new ParallelQuickSort();
3,743 sec