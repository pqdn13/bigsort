package edu.babanin.bigsort.util;

import java.util.Random;
import java.util.stream.IntStream;

public class Generate {
    public static int[] gen(int maxValue, int size){
        Random random = new Random();
        return IntStream.generate(() -> Math.abs(random.nextInt() % maxValue)).limit(size).toArray();
    }
}
