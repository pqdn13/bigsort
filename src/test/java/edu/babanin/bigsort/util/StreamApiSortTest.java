package edu.babanin.bigsort.util;

import org.junit.Test;

import java.util.Arrays;

public class StreamApiSortTest extends SortTest{
    @Test
    public void time() throws Exception {
        timeSort5Avr("stream api sort", (ints) -> Arrays.stream(ints).sorted().toArray());

        timeSort5Avr("streap api parallel sort", (ints) -> Arrays.stream(ints).parallel().sorted().toArray());
    }
}
